// const moment = require('moment');
// const _ = require('underscore');
//
//
// function getOnlyAlphabetChar(str = '') {
//   return str.replace(/[^a-zA-Z]/g, '');
// }
//
// function toBoolean(data) {
//   if (typeof data === 'boolean') return data;
//
//   if (!data) return false;
//   if (typeof data === 'string') {
//     if (data.toLowerCase() === 'y') return true;
//     if (data.toLowerCase() === 'n') return false;
//   }
//
//   if (typeof data === 'number') {
//     if (data === 0) return false;
//   }
//   return true;
// }
//
// function injectDataToStr({
//   preDelimiter = '{', subDelimiter = '}', orgStr, data,
// }) {
//   let result = orgStr;
//
//   Object.keys(data).forEach((k) => {
//     const regex = new RegExp(preDelimiter + k + subDelimiter, 'g');
//     result = result.replace(regex, data[k]);
//   });
//
//   return result;
// }
//
// function unixTimeToDate({ unixTime, format = 'YYYY.MM.DD HH:mm:ss', utc = false }) {
//   const unixToDate = moment.unix(unixTime);
//   return (utc) ? moment(unixToDate.utc()).format(format) : moment(unixToDate).format(format);
// }
//
// function toFormattedDate(date, format = 'YYYY.MM.DD') {
//   return moment(date).format(format);
// }
//
// function objCount(obj) {
//   return obj.length;
// }
//
// function userAgent(req) {
//   const agent = req.header('user-agent');
//   console.log(agent);
//   let os = '';
//   switch (true) {
//     case /Android/.test(agent):
//       os = 'aos';
//       break;
//
//     case /iPhone/.test(agent):
//       os = 'ios';
//       break;
//
//     case /iPad/.test(agent):
//       os = 'ios';
//       break;
//
//     default:
//       os = 'unknown';
//   }
//   return os;
// }
//
//
// function isFullString(t) {
//   try {
//     return isString(t) && t.trim().length > 0;
//   } catch (e) {
//     return false;
//   }
// }
//
// function getMiddle(s) {
//   if (s.length % 2 === 0) {
//     return (s.length > 2) ? s[(s.length / 2) - 1] + s[s.length / 2] : s[s.length / 2];
//   }
//   return s[Math.floor(s.length / 2)];
// }
//
// function toMaskedName(oriStr = '') {
//   return oriStr.replace(getMiddle(oriStr), '*');
// }
//
// function isNull(t) {
//   try {
//     return _.isNull(t);
//   } catch (e) {
//     return false;
//   }
// }
//
// function isArray(t, gte = 0) {
//   try {
//     return Array.isArray(t) && t.length >= gte;
//   } catch (e) { return false; }
// }
//
// function isObject(t) {
//   try {
//     return _.isObject(t) && !isArray(t);
//   } catch (e) {
//     return false;
//   }
// }
//
// function getS3Path({ location, date = Date.now(), fileName }) {
//   return `${location}/${toFormattedDate(date, 'YYYY')}/${toFormattedDate(date, 'MM')}/${toFormattedDate(date, 'DD')}/${fileName}`;
// }
//
// module.exports = {
//   isString,
//   isFullString,
//   isPositiveInteger,
//   isIncludeSequenceNumber,
//   isNumberString,
//   hasDuplicatedValue,
//   stringToNumber,
//   isNaN,
//   isUndefined,
//   getBiggerNumber,
//   addEllipsis,
//   toCurrencyFormat,
//   toAddressFormat,
//   toMaskedName,
//   toBigNumber,
//   unixTimeToDate,
//   removeSeperator,
//   toShortenBirth,
//   isNull,
//   isObject,
//   removeZeroInDecimal,
//   roundDown,
//   injectDataToStr,
//   toFormattedDate,
//   userAgent,
//   toBoolean,
//   baseProfileImg,
//   getOnlyAlphabetChar,
//   capitalize,
//   bappStatusNumToName,
//   getS3Path,
//   objCount,
//   songTitle,
// };

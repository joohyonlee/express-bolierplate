const map = require('lodash/map');
const flatten = require('lodash/flatten');

const ExtendableError = require('./ExtenableError');

class ValidationError extends ExtendableError {
  constructor(errors, options = {}) {
    super({
      code: options.code || 1000,
      message: `validation error: ${errors}`,
      errors,
      status: options.status || 400,
      isPublic: false,
    });

    this.flatten = options.flatten;
    this.statusText = options.statusText;

    Error.captureStackTrace(this, this.constructor.name);
  }

  toString() {
    return JSON.stringify(this.toJSON());
  }

  toJSON() {
    if (this.flatten) return flatten(map(this.errors, 'messages'));
    return {
      status: this.status,
      statusText: this.statusText,
      errors: this.errors,
    };
  }
}

module.exports = ValidationError;

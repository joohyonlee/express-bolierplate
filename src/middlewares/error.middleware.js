const httpStatus = require('http-status');
const APIError = require('@errors/APIError');
const ValidationError = require('@errors/ValidationError');
const { env } = require('@infra/vars');

/**
 * Error handler. Send stacktrace only during development
 * @public
 */
const handler = (err, req, res, next) => {
  const response = {
    code: err.code || 9001,
    message: err.message || httpStatus[err.status],
    errors: err.errors,
    stack: err.stack,
  };
  console.error(response);

  if (env !== 'development') {
    delete response.stack;
  }

  return res.format({
    json() {
      return res.status(err.status).json(response);
    },
  });
};
exports.handler = handler;

/**
 * If error is not an instanceOf APIError, convert it.
 * @public
 */
exports.converter = (err, req, res, next) => {
  let convertedError;

  if (err instanceof ValidationError) {
    convertedError = err;
  } else {
    convertedError = new APIError({
      code: err.code,
      message: err.message,
      status: err.status,
      stack: err.stack,
      errors: err.errors,
    });
  }

  return handler(convertedError, req, res, next);
};

exports.notFound = (req, res) => res.redirect('/error/404');

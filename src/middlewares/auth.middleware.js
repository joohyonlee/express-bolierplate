const ValidationError = require('@errors/ValidationError');

exports.isLoggedIn = (req, res, next) => {
  if (!req.isAuthenticated()) {
    throw new ValidationError('로그인 후 이용해주세요', { code: 401 });
  } else {
    next();
  }
};

const multer = require('multer');
const multerS3 = require('multer-s3');

const { s3 } = require('@infra/vars');
const { s3: s3Object } = require('@infra/s3');
const { getS3Path } = require('@utils/type.util');
const sharp = require('sharp');

// 이미지 저장경로, 파일명 세팅
exports.uploadFile = (location) => multer({
  storage: multerS3({
    s3: s3Object,
    bucket: s3.bucket, // 버킷 이름
    transforms: () => {
      sharp().resize(1920, 1080)
        .jpeg({
          progressive: true,
          quality: 70,
        });
    },
    contentType: multerS3.AUTO_CONTENT_TYPE, // 자동을 콘텐츠 타입 세팅
    acl: 'public-read', // 클라이언트에서 자유롭게 가용하기 위함
    key: (req, file, cb) => {
      cb(null, getS3Path({ fileName: file.originalname, location }));
    },
  }),
});

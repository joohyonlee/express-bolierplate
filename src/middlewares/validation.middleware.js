
const defaults = require('lodash/defaults');
const ValidationError = require('@errors/ValidationError');

const defaultOptions = {
  contextRequest: false,
  allowUnknownHeaders: true,
  allowUnknownBody: true,
  allowUnknownQuery: true,
  allowUnknownParams: true,
  allowUnknownCookies: true,
  status: 400,
  statusText: 'Bad Request',
};
let globalOptions = {};

const unknownMap = {
  headers: 'allowUnknownHeaders',
  body: 'allowUnknownBody',
  query: 'allowUnknownQuery',
  params: 'allowUnknownParams',
  cookies: 'allowUnknownCookies',
};
function validate(errors, request, schema) {
  if (!request) {
    errors.push('invalid request');
    return;
  }
  try {
    Object.keys(schema).forEach((key) => {
      const validated = schema[key].validate(request[key]);

      if (validated.error) {
        errors.push(validated.error.message);
      }
    });
  } catch (err) {
    errors.push(err.message);
  }
}

module.exports = (schema) => {
  if (!schema) throw new Error('Please provide a validation schema');

  return (req, res, next) => {
    const errors = [];

    // Set default options
    const options = defaults({}, schema.options || {}, globalOptions, defaultOptions);

    // NOTE: mutates `errors`
    ['headers', 'body', 'query', 'params', 'cookies'].forEach((key) => {
      const allowUnknown = options[unknownMap[key]];
      const entireContext = options.contextRequest ? req : null;
      if (schema[key]) {
        validate(errors, req[key], schema[key], key, allowUnknown, entireContext);
      }
    });

    if (errors && errors.length === 0) return next();

    return next(new ValidationError(errors, options));
  };
};

exports.ValidationError = ValidationError;

exports.options = (opts) => {
  if (!opts) {
    globalOptions = {};
    return;
  }

  globalOptions = defaults({}, globalOptions, opts);
};

/* eslint-disable */

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const logger = require('./logger');
const { mysql, env } = require('./vars');

const modelPath = `${__dirname}/../models`;

let sequelize;

const dbConfig = {
  ...mysql,
  dialect: 'mysql',
};

const { Op } = Sequelize;

if (env === 'production') {
  dbConfig.pool = {
    max: 60,
    min: 20,
    acquire: 30000,
    idle: 10000,
  };
} else {
  dbConfig.logger = true;
}

const db = {};

/**
 * Connect to mysql db
 *
 * @returns {object} Sequelize  connection
 * @public
 */
exports.connect = () => {
  sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, dbConfig);

  return sequelize
    .authenticate()
    .then(() => {
      fs.readdirSync(modelPath)
        .filter((f) => f.includes('.model.js'))
        .forEach((file) => {
          const Model = require(path.join(modelPath, file));
          const model = Model.init(sequelize, Sequelize);
          db[model.name] = model;
        });

      Object.values(db)
        .filter((model) => typeof model.associate === 'function')
        .forEach((model) => model.associate(db));

      db.sequelize = sequelize;
      db.Sequelize = Sequelize;
      return sequelize.sync();
    })
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch((err) => {
      logger.error(`Mysql connection error: ${err}`);
      process.exit(-1);
    });
};

exports.close = async () => {
  await sequelize.close();
};

exports.models = db;
exports.Op = Op;

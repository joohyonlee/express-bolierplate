const path = require('path');


const envFileName = (process.env.NODE_ENV === 'development') ? '.env' : `.env.${process.env.NODE_ENV}`;

require('dotenv-safe').load({
  path: path.join(__dirname, `../../${envFileName}`),
  sample: path.join(__dirname, '../../.env.example'),
  allowEmptyValues: true,
});

module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  jwtSecret: process.env.JWT_SECRET,
  jwtRfreshSecret: process.env.JWT_REFRESH_SECRET,
  jwtExpirationInterval: process.env.JWT_EXPIRATION_TIME,
  jwtRefreshExpirationInterval: process.env.JWT_REFRESH_EXPIRATION_TIME,
  mysql: {
    database: process.env.MYSQL_DATABASE,
    host: process.env.MYSQL_HOST,
    username: process.env.MYSQL_USERNAME,
    port: process.env.MYSQL_PORT,
    password: process.env.MYSQL_PASSWORD,
  },
  logs: process.env.NODE_ENV === 'production' ? 'combined' : 'dev',
  s3: {
    bucket: process.env.S3_BUCKET,
    accessKeyId: process.env.S3_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.S3_AWS_SECRET_ACCESS_KEY,
    region: 'ap-northeast-2',
  },
  kakao: {
    javascript: process.env.KAKAO_JAVASCRIPT_KEY,
    rest: process.env.KAKAO_REST_KEY,
    secret: process.env.KAKAO_SECRET_KEY,
    callbackUrl: process.env.KAKAO_CALLBACK_URL,
  },
  hostUrl: process.env.HOST_URL,
  configSession: {
    key: process.env.SESSION_KEY,
    secret: process.env.SESSION_SECRET_KEY,
  },
  // naver: {
  //   clientId: process.env.NAVER_CLIENT_ID,
  //   clientSecret: process.env.NAVER_CLIENT_SECRET,
  //   profileUrl: 'https://openapi.naver.com/v1/nid/me',
  //   callbackUrl: 'https://wouldustar.com/accounts/login/naver/callback',
  // },
  facebook: {
    appId: process.env.FACEBOOK_APP_ID,
    secret: process.env.FACEBOOK_SECRET_KEY,
    callbackUrl: 'https://highschoolstyleicon.co.kr/accounts/login/facebook/callback',
  },
  response: {
    success: false,
    message: null,
    data: null,
  },
  redisConfig: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
  },
};

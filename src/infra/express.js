const express = require('express');
require('express-async-errors');
const morgan = require('morgan');
const compress = require('compression');
const methodOverride = require('method-override');
const cors = require('cors');
const helmet = require('helmet');
const passport = require('passport');
const nunjucks = require('nunjucks');


const path = require('path');

const dateFilter = require('nunjucks-date-filter');
const requestIp = require('request-ip');
const useragent = require('express-useragent');
// const redis = require('redis');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const expressSession = require('cookie-session');


// const RedisStore = require('connect-redis')(session);


const error = require('@middlewares/error.middleware');

const {
  toFormattedDate, objCount, songTitle,
} = require('@utils/type.util');
const frontRoutes = require('../app/front/routes');
// const adminRoutes = require('../app/admin/routes');

const { logs, configSession, redisConfig } = require('./vars');


/**
 * Express instance
 * @public
 */
const app = express();

// request logging. dev: console | production: file
app.use(morgan(logs));
app.use(requestIp.mw());

app.use(cookieParser());


// passport configuration
// require('./passport').configurePassport(passport);

// parse body params and attache them to req.body
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// gzip compression
app.use(compress());

// lets you use HTTP verbs such as PUT or DELETE
// in places where the client doesn't support it
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());


// set view engine
app.set('view engine', 'html');
const nunjucksEnv = nunjucks
  .configure(path.join(__dirname, '../views'), {
    autoescape: true,
    express: app,
  });

nunjucksEnv.addGlobal('ENV', process.env);

nunjucksEnv.addFilter('date', dateFilter);

nunjucksEnv.addFilter('dateFormat', toFormattedDate);
nunjucksEnv.addFilter('objCount', objCount);

// nunjucksEnv.addFilter('baseProfileImg', baseProfileImg);
// nunjucksEnv.addFilter('capitalize', capitalize);
// nunjucksEnv.addFilter('bappStatusNumToName', bappStatusNumToName);

// set static files
app.use(express.static(path.join(__dirname, '../public')));


// const client = redis.createClient(redisConfig.port, redisConfig.host);

// passport session
// app.use(session({
//   store: new RedisStore({ client }),
//   key: configSession.key,
//   secret: configSession.secret,
//   saveUninitialized: false,
//   resave: false,
//   maxAge: 24 * 60 * 60 * 1000,
// }));


// const expiryDate = new Date(Date.now() + 7 * 24 * 60 * 60 * 1000); // 7 days

// const cookieSession = expressSession({
//   secret: configSession.secret,
//   resave: false,
//   saveUninitialized: true,
//   cookie: {
//     secureProxy: true,
//     httpOnly: true,
//     domain: 'example.com',
//     expires: expiryDate,
//   },
// });
//
// app.use(cookieSession);

app.use(useragent.express());

app.use((req, res, next) => {

  res.locals.user = req.user;
  if (req.user) {
    nunjucksEnv.addGlobal('user', req.user);
  }
  nunjucksEnv.addGlobal('userAgent', req.useragent);
  next();
});

// enable authentication
app.use(passport.initialize());
app.use(passport.session());


// mount page routes
app.use('/', frontRoutes);
// app.use('/cfnf8zV3SU7KjhHMDQ9T7Ms2ANxJjg', adminRoutes);

app.use(error.converter);
// app.use(error.notFound);

// error handler, send stacktrace only during development
app.use(error.handler);

module.exports = app;

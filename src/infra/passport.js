const LocalStrategy = require('passport-local').Strategy;
const NaverStrategy = require('passport-naver').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const KakaoStrategy = require('passport-kakao').Strategy;
const { models } = require('@infra/mysql');
const { kakao, naver, facebook } = require('@infra/vars');
const accountService = require('@services/account.service');


exports.configurePassport = (passport) => {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser(async (user, done) => {
    console.log(`login deserial: ${user}`);
    const findUser = await models.Account.findOne({ where: { id: user }, raw: true });
    if (findUser) {
      return done(null, findUser);
    }
    return done(null, false);
  });

  passport.use(new LocalStrategy({
    usernameField: 'accountId',
    passwordField: 'accountId',
    passReqToCallback: true, // 인증을 수행하는 인증 함수로 HTTP request를 그대로  전달할지 여부를 결정한다
  }, (async (req, username, password, done) => {
    console.log(`login id: ${username}`);
    if (username !== password) {
      return done(null, false);
    }
    const findUser = await models.Account.findOne({ where: { id: username }, raw: true });
    console.log(`passportuser: ${findUser.id}`);
    if (findUser) {
      return done(null, findUser);
    }
    return done(null, false);
  })));

  // passport.use(new NaverStrategy({
  //   clientID: naver.clientId,
  //   clientSecret: naver.clientSecret,
  //   callbackURL: naver.callbackUrl,
  // },
  // (async (accessToken, refreshToken, profile, done) => {
  //   const _profile = profile._json;
  //   const username = `naver_${_profile.id}`;
  //   const findQuery = { where: { username } };
  //   const findUser = await accountService.getAccount(findQuery);
  //   if (!findUser) {
  //     const insertQuery = {
  //       username,
  //       nickname: username,
  //       name: _profile.nickname,
  //       status: 1,
  //       type: 'naver',
  //       socialId: _profile.id,
  //       email: _profile.email,
  //       leftAt: null,
  //     };
  //     const createUser = await accountService.createTransaction(insertQuery);
  //     return done(null, createUser.defaultValues);
  //   }
  //   return done(null, findUser);
  // })));

  passport.use(new KakaoStrategy({
    clientID: kakao.javascript,
    callbackURL: kakao.callbackUrl,
  },
  (async (accessToken, refreshToken, profile, done) => {
    const _profile = profile._json;
    console.log(_profile)
    const username = `kakao_${_profile.id}`;
    const findQuery = { where: { username } };
    const findUser = await accountService.getAccount(findQuery);
    if (!findUser) {
      const insertQuery = {
        username,
        name: _profile.properties.nickname,
        nickname: username,
        profileImg:_profile.properties.profile_image,
        status: 1,
        type: 'kakao',
        socialId: _profile.id,
        email: _profile.kakao_account.email,
        leftAt: null,
      };
      const createUser = await accountService.createTransaction(insertQuery);
      console.log(createUser);
      return done(null, createUser.dataValues);
    }
    const email = _profile.kakao_account.email;
    const updateUser = await accountService.updateAccount({profileImg:_profile.properties.profile_image, email},{where:{id:findUser.id}})
    return done(null, findUser);
  })));

  passport.use(new FacebookStrategy({
    clientID: facebook.appId,
    clientSecret: facebook.secret,
    callbackURL: facebook.callbackUrl,
    profileFields: ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone',
      'updated_time', 'verified', 'displayName'],
  }, (async (accessToken, refreshToken, profile, done) => {
    const _profile = profile._json;
    console.log(_profile);

    const username = `facebook_${_profile.id}`;
    const findQuery = { where: { username } };
    const findUser = await accountService.getAccount(findQuery);
    if (!findUser) {
      const insertQuery = {
        username,
        status: 1,
        type: 'facebook',
        name: _profile.name,
        nickname: username,
        socialId: _profile.id,
        leftAt: null,
      };
      const createUser = await accountService.createTransaction(insertQuery);
      console.log(createUser);
      return done(null, createUser.dataValues);
    }
    return done(null, findUser);
  })));
};

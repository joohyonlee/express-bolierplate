const AWS = require('aws-sdk');

const { s3 } = require('@infra/vars');
const { getS3Path } = require('@utils/type.util');

const { promisify } = require('util');


AWS.config.update({
  accessKeyId: s3.accessKeyId,
  secretAccessKey: s3.secretAccessKey,
  region: 'ap-northeast-2',
});

const s3Obj = new AWS.S3();

exports.s3 = s3Obj;


exports.uploadFile = async ({ location, fileName, file }) => new Promise((resolve, reject) => {
  s3Obj.upload({
    Bucket: s3.bucket,
    Body: file,
    Key: getS3Path({ location, fileName }),
  }, (err, res) => {
    if (err) return reject(err);
    return resolve(res.Location);
  });
});

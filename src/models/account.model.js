const Sequelize = require('sequelize');

class Account extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        id: { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true },
        username: {
          type: DataTypes.STRING(100),
          unique: true,
          allowNull: false,
          validate: { notEmpty: true },
        },
        name: {
          type: DataTypes.STRING(100),
          allowNull: false,
          validate: { notEmpty: true },
        },
        nickname: {
          type: DataTypes.STRING(100),
          allowNull: false,
          validate: { notEmpty: true },
        },
        email: DataTypes.STRING(100),
        birth: {
          type: DataTypes.STRING(15),
          allowNull: true,
          validate: { notEmpty: true },
        },
        type: {
          type: DataTypes.STRING(15),
          allowNull: false,
          validate: { notEmpty: true },
        },
        socialId: {
          type: DataTypes.STRING(15),
          allowNull: false,
          validate: { notEmpty: true },
        },
        profileImg: DataTypes.STRING(255),
        leftAt: DataTypes.DATE,
        marketingAgree: DataTypes.INTEGER,
        status: { type: DataTypes.INTEGER, defaultValue: 0 },
      },
      {
        modelName: 'Account',
        tableName: 'account',
        sequelize,
        timestamps: true,
      },
    );
  }
}

module.exports = Account;

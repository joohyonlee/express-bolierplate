const express = require('express');
const passport = require('passport');

const router = express.Router();

// naver 로그인
router.get('/login/naver',
  passport.authenticate('naver'));
// naver 로그인 연동 콜백
router.get('/login/naver/callback',
  passport.authenticate('naver', {
    successRedirect: '/',
    failureRedirect: '/',
  }));

// kakao 로그인
router.get('/login/kakao',
  passport.authenticate('kakao'));
// kakao 로그인 연동 콜백
router.get('/login/kakao/callback',
  passport.authenticate('kakao', {
    successRedirect: '/',
    failureRedirect: '/',
  }));

// facebook 로그인
router.get('/login/facebook',
  passport.authenticate('facebook'));
// facebook 로그인 연동 콜백
router.get('/login/facebook/callback',
  passport.authenticate('facebook', {
    successRedirect: '/',
    failureRedirect: '/',
  }));


module.exports = router;

const express = require('express');

const { kakao, hostUrl } = require('@infra/vars');
const { isLoggedIn } = require('@middlewares/auth.middleware');

const accountController = require('../controllers/account.controller');

const router = express.Router();

// 로그인 관련
router.get('', (req, res) => res.render('index'));

module.exports = router;

const ValidationError = require('@errors/ValidationError');

exports.logout = async (req, res) => {
  req.logout();
  res.redirect('/');
};

const express = require('express');
const ERROR_CONTENTS = require('@errors/ERROR_CONTENTS');

const router = express.Router();

router.get('/:code', (req, res) => {
  const { code } = req.params;
  const err = ERROR_CONTENTS[code] || ERROR_CONTENTS[9001];

  res.format({
    html() {
      res.render('error', err);
    },
    json() {
      res.json({ status: false, code });
    },
  });
});


module.exports = router;

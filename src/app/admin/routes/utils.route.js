const { Router } = require('express');

const router = Router();

const { isLoggedIn } = require('@middlewares/auth.middleware');
const { uploadFile } = require('@middlewares/file.middleware');

router.use(isLoggedIn);
router.post('/upload', uploadFile('register').single('file'), (req, res, next) => {
  const reqFile = req.file;
  res.status(200).json({
    success: true,
    code: 200,
    data: {
      url: reqFile.location,
      originalname: reqFile.originalname,
      contentType: reqFile.contentType,
      size: reqFile.size,
    },
  });
});

module.exports = router;

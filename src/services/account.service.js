const request = require('request-promise-native');

const bcrypt = require('bcrypt');
const BootpayRest = require('bootpay-rest-client');
const { models } = require('@infra/mysql');
const { bootpay } = require('@infra/vars');
const getAge = require('get-age');
const ValidationError = require('@errors/ValidationError');

// Query로 회원(Account) 조회
exports.getAccount = async (qeurystring) => {
  const users = await models.Account.findOne(qeurystring);
  return users;
};

// Query로 회원(AccountInfo) 조회
exports.getAccountInfo = async (qeury) => {
  const user = await models.AccountInfo.findOne(qeury);
  return user;
};

exports.createAccount = async (query, transaction = null) => {
  const user = await models.Account.create(query, transaction);
  return user;
};

exports.createAccountInfo = async (query, transaction = null) => {
  const accountInfo = await models.AccountInfo.create(query, transaction);
  return accountInfo;
};

exports.updateAccount = async (query, whereQuery, transaction = null) => {
  if (transaction) {
    const accountInfo = await models.Account.update(query, whereQuery, transaction);
    return accountInfo;
  }
  const accountInfo = await models.Account.update(query, whereQuery);
  return accountInfo;
};

exports.updateAccountInfo = async (query, whereQuery, transaction = null) => {
  if (transaction) {
    const accountInfo = await models.Account.update(query, whereQuery, transaction);
    return accountInfo;
  }
  const accountInfo = await models.AccountInfo.update(query, whereQuery);
  return accountInfo;
};


// 생년월일 문자를 -> Date type 으로 변경
exports.birthParse = (str) => {
  const y = str.substr(0, 4);
  const m = str.substr(4, 2);
  const d = str.substr(6, 2);
  const newDate = `${y}-${m}-${d}`;

  const dateBirth = new Date(newDate);
  const age = getAge(dateBirth);

  if (age < 19) throw new ValidationError('19세 미만 이용자 입니다.');
  dateBirth.setDate(dateBirth.getDate());
  return str;
};


// exports.importWallet = async (pubKey, pinCode, accountId) => {
//   const hasWallet = await models.Wallet.hasWallet({ accountId, coinIds: [2] });
//   if (hasWallet) throw new Error('이미 지갑이 존재합니다.');
//   const keyPair = await custodyService.validateWalletKey(pubKey);
//   if (keyPair.isValid === false) throw new ValidationError('입력하신 Key가 유효하지 않습니다. 확인 후 다시 입력해주세요.');

//   const isDuplicated = await walletService
//     .checkForDuplicateAddress({ address: keyPair.targetAddress });
//   if (isDuplicated) throw new ValidationError('등록된 지갑이 있습니다.');
//   const importedAccount = await custodyService.importAccount({ walletKey: pubKey, pinCode });
//   // TODO : throw error when importedAccount is undefined
//   // if (importedAccount) throw Exception('')

//   const parentCoin = await models.Coin.getParentCoin();

//   const params = {
//     accountId,
//     balance: 0,
//     address: importedAccount.address,
//     coinId: parentCoin.id,
//     status: 1,
//   };

//   const historyParams = {
//     accountId,
//     address: importedAccount.address,
//     status: 1,
//     type: 'I',
//     txid: importedAccount.transaction_id,
//     publicKey: importedAccount.publicKey,
//   };

//   // TODO : transaction
//   const pubkeyWallet = await models.Wallet.create(params);
//   await walletHistoryService.createWalletHistory(historyParams);
//   await this.updateAccountStatus({ accountId, status: 1 });
//   return pubkeyWallet;
// };

// exports.exportWallet = async (address, newPublicKey, pinCode, accountId) => {
//   const exportedAccount = await custodyService.exportAccount({ address, newPublicKey, pinCode });
//   const historyParams = {
//     accountId,
//     address: exportedAccount.address,
//     status: 1,
//     type: 'I',
//     txid: exportedAccount.transaction_id,
//     publicKey: exportedAccount.publicKey,
//   };
//     // TODO : transaction
//   await walletHistoryService.createWalletHistory(historyParams);
//   return exportedAccount;
// };

// exports.addAccountActionHistory = async (query) => {
//   const coin = await models.AccountActionHistory.create(query);
//   return coin;
// };

exports.updateTransaction = async (data) => {
  await models.sequelize.transaction(async (t) => {
    await this.updateAccount(data.query, data.whereQuery, { transaction: t });
    await this.updateAccountInfo(data.infoQuery, data.infoWhereQuery, { transaction: t });
  });
};

exports.createTransaction = async (insertQuery) => {
  const result = await models.sequelize.transaction(async (t) => {
    const createUser = await this.createAccount(insertQuery, { transaction: t });
    const accountId = createUser.dataValues.id;
    console.log(accountId);
    await this.createAccountVote(accountId, { transaction: t });
    await this.createAccountVoteHistory(accountId, { transaction: t });
    return createUser;
  });
  return result;
};

exports.createAccountVote = async (accountId, transaction = null) => {
  const result = await models.Votes.create({
    accountId,
    status: 1,
    hasVotes: 3,

  }, transaction);
  return result;
};

exports.createAccountVoteHistory = async (accountId, transaction = null) => {
  const result = await models.VotesHistory.create({
    accountId,
    status: 1,
    type: 0,
    votes: 3,

  }, transaction);
  return result;
};

exports.sessionLogin = async (req, user, nickname, token, profileImage) => {
  try {
    req.login(user, (err2) => {
      if (err2) throw err2;
      req.session.passport.nickName = nickname;
      req.session.passport.token = token;
      req.session.passport.profileImg = profileImage;
    });
    return user;
  } catch (err) {
    throw err;
  }
};

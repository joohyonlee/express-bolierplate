files:
  "/etc/nginx/nginx.conf":
    mode: "000644"
    owner: root
    group: root
    content: |
      user                    nginx;
      error_log               /var/log/nginx/error.log warn;
      pid                     /var/run/nginx.pid;
      worker_processes        auto;
      worker_rlimit_nofile    200000;

      events {
        use epoll;
        worker_connections  8192;
        multi_accept on;
      }

      http {
        include       /etc/nginx/mime.types;
        default_type  application/octet-stream;

        open_file_cache max=200000 inactive=20s;
        open_file_cache_valid 30s;
        open_file_cache_min_uses 2;
        open_file_cache_errors on;

        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
        '$status $body_bytes_sent "$http_referer" '
        '"$http_user_agent" "$http_x_forwarded_for"';

        access_log  /var/log/nginx/access.log  main;

        sendfile        on;
        # send headers in one piece, it is better than sending them one by one
        tcp_nopush on;

        # don't buffer data sent, good for small data bursts in real time
        tcp_nodelay on;
        reset_timedout_connection on;
        client_body_timeout   10;
        send_timeout 2;
        keepalive_timeout     30;

        # Elastic Beanstalk Modification(EB_INCLUDE)
        log_format healthd '$msec"$uri"'
        '$status"$request_time"$upstream_response_time"'
        '$http_x_forwarded_for';

        # include /etc/nginx/conf.d/*.conf;
        map $http_upgrade $connection_upgrade {
          default "upgrade";
        }

        upstream nodejs {
          server 127.0.0.1:8081;
          keepalive 1024;
        }

        server {
          listen 8080;

          if ($time_iso8601 ~ "^(\d{4})-(\d{2})-(\d{2})T(\d{2})") {
            set $year $1;
            set $month $2;
            set $day $3;
            set $hour $4;
          }
          access_log  /var/log/nginx/healthd/application.log.$year-$month-$day-$hour healthd;
          access_log  /var/log/nginx/access.log  main;

          location / {
            proxy_pass  http://nodejs;
            proxy_http_version 1.1;
            proxy_set_header  Connection $connection_upgrade;
            proxy_set_header  Upgrade $http_upgrade;
            proxy_set_header  Host  $host;
            proxy_set_header  X-Real-IP $remote_addr;
            proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
            client_max_body_size 20M;
          }

          #client_header_timeout 60;
          reset_timedout_connection on;
          client_body_timeout   10;
          send_timeout 2;
          keepalive_timeout     30;
          gzip on;
          gzip_comp_level 4;
          gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
        }
      }

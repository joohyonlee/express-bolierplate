# express-BoilerPlate

## Features

## Requirements

- [Node v8.6+](https://nodejs.org/en/download/current/)
- [Yarn](https://yarnpkg.com/en/docs/install)

## Getting Started

Clone the repo and make it yours:

```bash
git clone https://joohyonlee@bitbucket.org/joohyonlee/express-bolierplate.git
cd express-boilerplate
```

Install dependencies:

```bash
npm install
```

Set environment variables:

```bash
cp .env.example .env
```

## Running Locally

```bash
npm run dev
```

## Running in Production

```bash
npm start
```

## Lint

```bash
# lint code with ESLint
npm run lint

# try to fix ESLint errors
npm run lint:fix

# lint and watch for changes
npm run lint:watch
```

## Test

```bash
# run all tests with jest
npm run test

# run all tests and watch for changes
npm run test:watch

# open nyc test coverage reports
npm run test:cov
```

## Validate

```bash
# run lint and tests
npm run validate
```

## Logs

```bash
# show logs in production
pm2 logs
```

## Documentation

```bash
# generate and open api documentation
npm run docs
```

## License
